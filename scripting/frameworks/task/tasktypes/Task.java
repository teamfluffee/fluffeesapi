package scripts.modules.fluffeesapi.scripting.frameworks.task.tasktypes;

import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.TaskFinish;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.INode;
import scripts.modules.fluffeesapi.scripting.frameworks.task.Priority;

/**
 * @author Encoded
 */
public abstract class Task implements INode {

    public abstract Priority priority();
    public abstract boolean isValid();
    public abstract TaskFinish execute();
    public abstract String getStatus();

    public INode getValidNode() {
        return this;
    }
}