package scripts.modules.fluffeesapi.scripting.frameworks.task;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW,
    NONE
}
