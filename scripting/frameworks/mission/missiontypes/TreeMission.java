package scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes;

import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.INode;

public interface TreeMission extends Mission {

    BaseDecisionNode getTreeRoot();

    default INode getCurrentNode() {
        return getTreeRoot().getValidNode();
    }
}