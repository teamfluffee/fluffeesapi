package scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.taskmissions;

import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.INode;
import scripts.modules.fluffeesapi.scripting.frameworks.task.TaskSet;

public interface TaskMission extends Mission {

    TaskSet getTaskSet();

    default INode getCurrentNode() {
        return getTaskSet().getValidTask();
    }
}
