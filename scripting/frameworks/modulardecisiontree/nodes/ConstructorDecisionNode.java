package scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes;

public abstract class ConstructorDecisionNode extends BaseDecisionNode {

    public ConstructorDecisionNode() {
        initializeNode();
    }
}
