package scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes;

import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.TaskFinish;

public abstract class SuccessProcessNode extends ProcessNode {

    public abstract void successExecute();

    @Override
    public TaskFinish execute() {
        successExecute();
        return NodeFinishes.GENERIC_SUCCESS;
    }
}
