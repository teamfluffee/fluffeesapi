package scripts.modules.fluffeesapi.data.progression;

public interface TrainingMethod {

    String getName();

}
