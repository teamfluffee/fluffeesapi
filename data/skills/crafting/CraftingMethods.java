package scripts.modules.fluffeesapi.data.skills.crafting;

import scripts.modules.fluffeesapi.data.interactables.InteractableItem;

public interface CraftingMethods {
    InteractableItem[] getCraftingEquipment();
}
