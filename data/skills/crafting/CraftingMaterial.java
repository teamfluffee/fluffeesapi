package scripts.modules.fluffeesapi.data.skills.crafting;

import scripts.modules.fluffeesapi.data.interactables.Interactable;

public interface CraftingMaterial {
    Interactable getMaterial();
}
